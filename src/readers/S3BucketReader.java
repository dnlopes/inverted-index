package readers;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import edu.cmu.lemurproject.WarcRecord;
import edu.cmu.lemurproject.WritableWarcRecord;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;


/**
 * Created by dnlopes on 30/11/14. This class reads a whole bucket from Amazon S3.
 */
public class S3BucketReader extends RecordReader<LongWritable, WritableWarcRecord>
{

	private static final String BUCKET_PREFIX = "input/plain/";

	private static final String BUCKET_NAME = "scc-titas";
	private static final String ACCESS_KEY_ID = "AKIAIM55SKAZ4OL5EYKA";
	private static final String SECRET_KEY = "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";


	private LongWritable key;
	private WritableWarcRecord value;
	private int totalFiles, totalFilesProccessed;
	private boolean finished;
	private List<S3ObjectSummary> objSummaries;
	private AmazonS3 s3;
	private DataInputStream objectInputStream;


	public S3BucketReader(int max)
	{
		if(max > 10)
			this.totalFiles = 10;
		else
			this.totalFiles = max;

		this.finished = false;
		this.totalFilesProccessed = 0;
		this.key = new LongWritable();
		this.value = new WritableWarcRecord();
		this.objSummaries = new LinkedList<>();

		AWSCredentials awsCreds = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY);

		this.s3 = new AmazonS3Client(awsCreds);

	}

	@Override
	public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException
	{
		ObjectListing objList = s3.listObjects(BUCKET_NAME, BUCKET_PREFIX);

		do
		{
			List<S3ObjectSummary> summaries = objList.getObjectSummaries();

			this.objSummaries.addAll(summaries);
			objList = s3.listNextBatchOfObjects(objList);
			System.out.println("ObjectList size: " + this.objSummaries.size());

		} while(this.objSummaries.size() < this.totalFiles);

		this.objectInputStream = this.openNextFile();
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException
	{
		if(finished)
			return false;

		WarcRecord thisWarcRecord = WarcRecord.readNextWarcRecord(this.objectInputStream);

		if(thisWarcRecord == null)
		{
			this.totalFilesProccessed++;
			// go to next file or exit if all files were proccessed
			if(this.totalFilesProccessed == this.totalFiles)
			{
				this.finished = true;
				return false;
			}
			else
			{
				this.objectInputStream = this.openNextFile();
				thisWarcRecord = WarcRecord.readNextWarcRecord(this.objectInputStream);
			}
		}

		// if its null here something went wrong
		if(thisWarcRecord != null)
		{
			this.value.setRecord(thisWarcRecord);
			this.key = new LongWritable(this.key.get() + 1);
			return true;
		} else
		{
			System.out.println("WarcRecord is null and it should not be!");
			return false;
		}
	}

	@Override
	public LongWritable getCurrentKey() throws IOException, InterruptedException
	{
		return this.key;
	}

	@Override
	public WritableWarcRecord getCurrentValue() throws IOException, InterruptedException
	{
		return this.value;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException
	{
		return 0;
	}

	@Override
	public void close() throws IOException
	{
	}

	private DataInputStream openNextFile()
	{
		S3ObjectSummary summary = this.objSummaries.get(totalFilesProccessed);
		String key = summary.getKey();

		S3Object object = this.s3.getObject(new GetObjectRequest(BUCKET_NAME, key));

		try
		{
			InputStream objectData = object.getObjectContent();
			GZIPInputStream gzInputStream = new GZIPInputStream(objectData);
			return new DataInputStream(gzInputStream);

		} catch(IOException e)
		{
			return null;
		}
	}
}
