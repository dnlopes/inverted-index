/**
 * A Hadoop record reader for reading Warc Records
 *
 * (C) 2009 - Carnegie Mellon University
 *
 * 1. Redistributions of this source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. The names "Lemur", "Indri", "University of Massachusetts",
 *    "Carnegie Mellon", and "lemurproject" must not be used to
 *    endorse or promote products derived from this software without
 *    prior written permission. To obtain permission, contact
 *    license@lemurproject.org.
 *
 * 4. Products derived from this software may not be called "Lemur" or "Indri"
 *    nor may "Lemur" or "Indri" appear in their names without prior written
 *    permission of The Lemur Project. To obtain permission,
 *    contact license@lemurproject.org.
 *
 * THIS SOFTWARE IS PROVIDED BY THE LEMUR PROJECT AS PART OF THE CLUEWEB09
 * PROJECT AND OTHER CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author mhoy@cs.cmu.edu (Mark J. Hoy)
 * @modified by David Lopes
 */

package edu.cmu.lemurproject;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.util.ReflectionUtils;

import java.io.DataInputStream;
import java.io.IOException;


//public class WarcFileRecordReader<K extends LongWritable, V extends Writable>  extends RecordReader<LongWritable, WritableWarcRecord> {
public class WarcFileRecordReader extends RecordReader<LongWritable, WritableWarcRecord>
{

	private Path[] filePathList = null;
	private int currentFilePath = - 1;
	private long recordNumber = 0;
	private int totalFiles, numberFilesProccessed;
	private boolean finished;

	private FSDataInputStream currentFile = null;
	private CompressionCodec compressionCodec = null;
	private DataInputStream compressionInput = null;

	private long totalFileSize = 0;
	private long totalNumBytesRead = 0;

	private final LongWritable key = new LongWritable();
	private final WritableWarcRecord value = new WritableWarcRecord();
	private Configuration conf;

	@Override
	public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException
	{
		this.finished = false;
		this.numberFilesProccessed = 0;
		this.conf = context.getConfiguration();
		this.totalFiles = conf.getInt("numberFiles", 1);

		System.out.println("Number of files to process: " + this.totalFiles);
		//this.fs = FileSystem.get(conf);

		if(split instanceof FileSplit)
		{
			this.filePathList = new Path[1];
			this.filePathList[0] = ((FileSplit) split).getPath();
		} else
		{
			throw new IOException("InputSplit is not a file split or a multi-file split - aborting");
		}

		// get the total file sizes
		for(int i = 0; i < filePathList.length; i++)
		{
			//totalFileSize += fs.getFileStatus(filePathList[i]).getLen();
			totalFileSize += filePathList[i].getFileSystem(conf).getFileStatus(filePathList[i]).getLen();
		}

		Class<? extends CompressionCodec> codecClass = null;

		try
		{
			codecClass = conf.getClassByName("org.apache.hadoop.io.compress.GzipCodec").asSubclass(CompressionCodec.class);
			compressionCodec = (CompressionCodec) ReflectionUtils.newInstance(codecClass, conf);
		} catch(ClassNotFoundException cnfEx)
		{
			compressionCodec = null;
		}

		openNextFile();
	}

	private boolean openNextFile()
	{
		this.numberFilesProccessed++;

		if(this.numberFilesProccessed > this.totalFiles)
		{
			this.finished = true;
			return false;
		}

		try
		{
			if(compressionInput != null)
			{
				compressionInput.close();
			} else if(currentFile != null)
			{
				currentFile.close();
			}
			currentFile = null;
			compressionInput = null;

			currentFilePath++;
			if(currentFilePath >= filePathList.length)
			{
				return false;
			}

			currentFile = filePathList[currentFilePath].getFileSystem(conf).open(filePathList[currentFilePath]);

			// is the file gzipped?
			if((compressionCodec != null) && (filePathList[currentFilePath].getName().endsWith("gz")))
			{
				compressionInput = new DataInputStream(compressionCodec.createInputStream(currentFile));
			}

		} catch(IOException ex)
		{
			return false;
		}
		return true;
	}

	public boolean nextKeyValue() throws IOException
	{
		if(finished)
			return false;

		DataInputStream whichStream = null;
		if(compressionInput != null)
		{
			whichStream = compressionInput;
		} else if(currentFile != null)
		{
			whichStream = currentFile;
		}

		if(whichStream == null)
		{
			return false;
		}

		WarcRecord newRecord = WarcRecord.readNextWarcRecord(whichStream);
		if(newRecord == null)
		{
			// try advancing the file
			if(openNextFile())
			{
				newRecord = WarcRecord.readNextWarcRecord(whichStream);
			}

			if(newRecord == null)
			{
				return false;
			}
		}

		totalNumBytesRead += (long) newRecord.getTotalRecordLength();
		newRecord.setWarcFilePath(filePathList[currentFilePath].toString());

		// now, set our output variables
		value.setRecord(newRecord);
		key.set(recordNumber);

		recordNumber++;
		return true;
	}

	public void close() throws IOException
	{
		totalNumBytesRead = totalFileSize;
		if(compressionInput != null)
		{
			compressionInput.close();
		} else if(currentFile != null)
		{
			currentFile.close();
		}
	}

	public float getProgress() throws IOException
	{
		if(compressionInput != null)
		{
			if(filePathList.length == 0)
			{
				return 1.0f;
			}
			// return which file - can't do extact byte matching
			return (float) currentFilePath / (float) (filePathList.length);
		}
		if(totalFileSize == 0)
		{
			return 0.0f;
		}
		return (float) totalNumBytesRead / (float) totalFileSize;
	}

	@Override
	public LongWritable getCurrentKey() throws IOException, InterruptedException
	{
		return key;
	}

	@Override
	public WritableWarcRecord getCurrentValue() throws IOException, InterruptedException
	{
		return value;
	}


}
