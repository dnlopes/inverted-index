package applications;

import edu.cmu.lemurproject.WarcFileInputFormat;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import mapred.*;

/**
 * Created by dnlopes on 27/11/14.
 */
public class InvertedIndex
{
	//private static final String ACCESS_KEY = "AKIAIM55SKAZ4OL5EYKA";
	//private static final String SECRET_KEY = "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";

	public static void main(String[] args) throws Exception
	{
		// args[0] input
		// args[1] output
		// args[2] localProccess (yes/no) (not yet used)
		// args[3] numberOfFiles
		// args[4] outputDirName

		if(args.length != 5)
		{
			System.out.println("Parameters: input output localProccess numberOfFiles outputDirName");
			System.exit(- 1);
		}
		Job conf = new Job(new Configuration(), "index");

		conf.getConfiguration().setInt("numberFiles", Integer.parseInt(args[3]));
		conf.setJarByClass(InvertedIndex.class);
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		conf.setMapperClass(MyMap.class);
		conf.setReducerClass(MyReduce.class);

		conf.setInputFormatClass(WarcFileInputFormat.class);
		conf.setOutputFormatClass(TextOutputFormat.class);


		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1] + "/" + args[4]));

		conf.waitForCompletion(true); // submit and wait
	}


}
