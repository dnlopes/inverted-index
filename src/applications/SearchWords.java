package applications;

import mapred.SearchMap;
import mapred.SearchReduce;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import writables.WordMetadataWritable;


/**
 * Created by dnlopes on 17/12/14.
 */
public class SearchWords
{
	public static void main(String[] args) throws Exception
	{
		// args[0] input
		// args[1] output

		if(args.length < 3)
		{
			System.out.println("Parameters: indexLocation outputDir [word_1,..., word_n]");
			System.exit(- 1);
		}

		String[] words = new String[args.length-2];

		System.arraycopy(args,2,words,0, words.length);

		Job conf = new Job(new Configuration(), "index");
		conf.getConfiguration().setStrings("words", words);

		conf.setJarByClass(SearchWords.class);

		//default output
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		//custom output for map
		conf.setMapOutputKeyClass(IntWritable.class);
		conf.setMapOutputValueClass(WordMetadataWritable.class);

		conf.setMapperClass(SearchMap.class);
		conf.setReducerClass(SearchReduce.class);

		conf.setInputFormatClass(TextInputFormat.class);
		conf.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		conf.waitForCompletion(true); // submit and wait


	}

}
