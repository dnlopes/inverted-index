package writables;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


/**
 * Created by dnlopes on 17/12/14.
 */
public class WordMetadataWritable implements Writable
{

	private Text word;
	private MapWritable metadata;


	public WordMetadataWritable()
	{
		this.word = new Text();
		this.metadata = new MapWritable();
	}

	public WordMetadataWritable(Text word, MapWritable metadata)
	{
		this.word = word;
		this.metadata = metadata;
	}


	@Override
	public void write(DataOutput dataOutput) throws IOException
	{
		this.word.write(dataOutput);
		this.metadata.write(dataOutput);
	}

	@Override
	public void readFields(DataInput dataInput) throws IOException
	{
		this.word.readFields(dataInput);
		this.metadata.readFields(dataInput);

		if(! this.isRecordValid())
			System.out.print("Error: failed to deserialize WordMetadataRecord");
	}


	private void parseRawMetadata(String rawData)
	{
		this.metadata = new MapWritable();

		String[] splitted = rawData.split("\t\t");
		this.word = new Text(splitted[0]);

		String[] metadataSplitted = splitted[1].split("\t");

		for(int i = 0; i < metadataSplitted.length; i += 2)
			this.metadata.put(new Text(metadataSplitted[i]), new IntWritable(Integer.parseInt(metadataSplitted[i + 1])));
	}

	private boolean isRecordValid()
	{
		if(this.word == null || this.metadata == null)
			return false;

		return true;
	}

	public MapWritable getMetadata()
	{
		return this.metadata;
	}

	public void feedRawData(String raw)
	{
		this.parseRawMetadata(raw);

		if(! this.isRecordValid())
			System.out.print("Error: failed to create WordMetadataRecord");
	}
}
