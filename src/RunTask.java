import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.*;
import com.amazonaws.services.elasticmapreduce.util.StepFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;


/**
 * Created by dnlopes on 12/12/14.
 */
public class RunTask
{

	private static final String S3_BUCKET_BASE_DIR = "s3n://scc-titas";
	private static final String ACCESS_KEY = "AKIAIM55SKAZ4OL5EYKA";
	private static final String SECRET_KEY = "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";

	public static void main(String[] args)
	{
		if(args.length != 1)
		{
			System.out.println("Wrong arguments");
			System.exit(- 1);
		}

		String[] splitted = args[0].split("/");
		String emrName = splitted[1];

		Properties prop = new Properties();
		InputStream input = null;

		try
		{

			input = new FileInputStream(args[0]);
			prop.load(input);

			if(! (prop.containsKey("instancetype") && prop.containsKey("input") && prop.containsKey("output") && prop.containsKey("localProccess") && prop.containsKey("numberFiles") && prop.containsKey("instances")))
			{
				System.out.println("Fatal error: configuration file missing parameters");
				System.exit(- 1);
			}

		} catch(IOException ex)
		{
			System.out.println("Fatal error: failed to load properties file");
			System.exit(- 1);
		} finally
		{
			if(input != null)
			{
				try
				{
					input.close();
				} catch(IOException e)
				{
					System.out.println("Fatal error: failed to close properties file");
					System.exit(- 1);
				}
			}
		}

		// args[0] input
		// args[1] output
		// args[2] localProccess (yes/no) (not yet used)
		// args[3] numberOfFiles
		// args[4] outputDirName

		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
		AmazonElasticMapReduceClient emr = new AmazonElasticMapReduceClient(credentials);
		emr.setRegion(Region.getRegion(Regions.US_WEST_2));
		StepFactory stepFactory = new StepFactory();

		//Adds a step to copy input files from S3 to HDFS if running locally---
		HadoopJarStepConfig customStep = new HadoopJarStepConfig("/home/hadoop/lib/emr-s3distcp-1.0.jar");
		Collection<String> distcpArgs = new ArrayList<>();
		distcpArgs.add("--src");
		distcpArgs.add(prop.getProperty("input"));
		distcpArgs.add("--dest");
		distcpArgs.add("hdfs:///input/");
		customStep.setArgs(distcpArgs);
		StepConfig s3DistcpTaks = new StepConfig().withName("S3DistCp step").withHadoopJarStep(customStep);

		// create custom task and specify parameters
		HadoopJarStepConfig customJarStep = new HadoopJarStepConfig(S3_BUCKET_BASE_DIR + "/main.jar");
		customJarStep.setMainClass("applications.InvertedIndex");

		Collection<String> argsList = new ArrayList<>();
		// HDFS
		if (prop.getProperty("localProccess").compareTo("yes") == 0)
			argsList.add("hdfs:///input");
		else
			argsList.add(prop.getProperty("input"));

		argsList.add(prop.getProperty("output"));
		argsList.add(prop.getProperty("localProccess"));
		argsList.add(prop.getProperty("numberFiles"));
		argsList.add(emrName);

		customJarStep.setArgs(argsList);
		StepConfig customTask = new StepConfig().withName("InvertedIndex-" + args[0]).withHadoopJarStep(customJarStep);

		// debug task
		StepConfig enabledebugging = new StepConfig().withName("Enable debugging").withActionOnFailure("TERMINATE_JOB_FLOW").withHadoopJarStep(stepFactory.newEnableDebuggingStep());

		JobFlowInstancesConfig clusterSpecs = new JobFlowInstancesConfig();
		clusterSpecs.setEc2KeyName("cloud-keypair");
		clusterSpecs.setHadoopVersion("1.0.3");
		clusterSpecs.setInstanceCount(Integer.parseInt(prop.getProperty("instances")));
		clusterSpecs.setKeepJobFlowAliveWhenNoSteps(false);
		clusterSpecs.setMasterInstanceType(prop.getProperty("instancetype"));
		clusterSpecs.setSlaveInstanceType(prop.getProperty("instancetype"));

		RunJobFlowRequest request = new RunJobFlowRequest();
		request.setAmiVersion("2.4.9");
		request.setName(emrName);
		request.setInstances(clusterSpecs);
		request.setLogUri(S3_BUCKET_BASE_DIR + "/logs/");

		Collection<StepConfig> stepsList = new ArrayList<>();
		stepsList.add(enabledebugging);

		//HDFS
		if (prop.getProperty("localProccess").compareTo("yes") == 0)
			stepsList.add(s3DistcpTaks);

		stepsList.add(customTask);
		request.setSteps(stepsList);

		emr.runJobFlow(request);
	}
}
