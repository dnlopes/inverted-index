package mapred;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import writables.WordMetadataWritable;

import java.io.IOException;

/**
 * Created by dnlopes on 17/12/14.
 */
public class SearchMap extends Mapper<LongWritable, Text, IntWritable, WordMetadataWritable>
{
	private String[] searchWord;
	private final static IntWritable ONE = new IntWritable(1);


	@Override
	public void setup(Context cont) throws IOException, InterruptedException
	{
		super.setup(cont);
		this.searchWord = cont.getConfiguration().getStrings("words");

	}

	public void map(LongWritable key, Text value, Mapper.Context cont) throws IOException, InterruptedException
	{
		int index = value.toString().indexOf("\t\t");
		String inputWord = value.toString().substring(0,index);

		for(String word : this.searchWord)
		{
			if(inputWord.compareTo(word) == 0)
			{
				WordMetadataWritable wordMetadata = new WordMetadataWritable();
				wordMetadata.feedRawData(value.toString());
				cont.write(ONE, wordMetadata);
				break;
			}
		}
	}
}

