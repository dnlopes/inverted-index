package mapred;

import edu.cmu.lemurproject.WarcRecord;
import edu.cmu.lemurproject.WritableWarcRecord;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.*;


/**
 * Created by dnlopes on 24/11/14.
 */

// receives (Long, WarRecord) and sends (Text (url), Map)
public class MyMap extends Mapper<LongWritable, WritableWarcRecord, Text, Text>
{

	public void map(LongWritable key, WritableWarcRecord value, Context cont) throws IOException, InterruptedException
	{
		Map<String, Integer> map = new HashMap<>();

		WarcRecord val = value.getRecord();

		// fill URL
		String stringURL = val.getHeaderMetadataItem("WARC-Target-URI").trim();

		if(stringURL.compareTo("") == 0)
			return;

		String[] parts = val.getContentUTF8().split(" ");

		for(String word : parts)
		{
			word = word.trim();

			if((word.length() < 4) || (! word.matches("[a-zA-Z]+")))
				continue;

			Integer counter = 0;

			if(map.containsKey(word))
				counter = map.get(word);

			counter += 1;
			map.put(word, counter);
		}

		// start to send stuff for reduce
		try
		{
			for(String word : map.keySet())
			{
				String pair = stringURL + "\t" + map.get(word).toString();
				cont.write(new Text(word), new Text(pair));
			}
		} catch(Exception e)
		{
			throw new RuntimeException(e.getMessage());
		}
	}
}
