package mapred;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Reducer;
import writables.WordMetadataWritable;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


/**
 * Created by dnlopes on 17/12/14.
 */
public class SearchReduce extends Reducer<IntWritable, WordMetadataWritable, Text, Text>
{

	public void reduce(IntWritable key, Iterable<WordMetadataWritable> values, Context cont) throws IOException, InterruptedException
	{
		Iterator<WordMetadataWritable> it = values.iterator();
		MapWritable result = new MapWritable(it.next().getMetadata());

		MapWritable temp;

		WordMetadataWritable next;
		while(it.hasNext())
		{
			next = it.next();
			temp = next.getMetadata();
			result = this.intersect(result, temp);
		}

		for(Map.Entry<Writable, Writable> entry : result.entrySet())
		{
			Text link = (Text) entry.getKey();
			IntWritable counter = (IntWritable) entry.getValue();

			cont.write(link, new Text(counter.toString()));
		}
	}

	private MapWritable intersect(MapWritable a, MapWritable b)
	{
		MapWritable intersection = new MapWritable();

		for(Map.Entry<Writable, Writable> entry : a.entrySet())
		{
			if(b.containsKey(entry.getKey()))
			{
				IntWritable counter1 = (IntWritable) a.get(entry.getKey());
				IntWritable counter2 = (IntWritable) b.get(entry.getKey());
				intersection.put(entry.getKey(), new IntWritable(counter1.get() + counter2.get()));
			}
		}
		return intersection;
	}
}
