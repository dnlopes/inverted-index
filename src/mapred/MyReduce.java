package mapred;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by dnlopes on 24/11/14.
 */

public class MyReduce extends Reducer<Text, Text, Text, Text>
{

	public void reduce(Text key, Iterable<Text> values, Context cont) throws IOException, InterruptedException
	{
		String finalString = "\t";

		for (Text pair: values)
			finalString += pair + "\t";

		cont.write(key, new Text(finalString));
	}
}