#!/bin/bash

CONFIG_FILE=$2

run()
{
	java -cp main.jar:libs/*:$HADOOP_PREFIX/lib/*: RunTask "$CONFIG_FILE"
	exit 0
}

upload()
{
  s3cmd rm s3://scc-titas/main.jar
  s3cmd put main.jar s3://scc-titas/  
}

sync()
{
  s3cmd sync s3://scc-titas/logs .
}

case "$1" in
  run)
        run
        ;;
  upload)
        upload
        ;;        
  sync)
        sync
        ;;        
  *)        echo "Usage: upload | sync | run [configFile]"
esac
