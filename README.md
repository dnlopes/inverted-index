# Hadoop - Inverted Index

## Dependencies

Make sure you set an environment variable called `HADOOP_PREFIX` pointing to Hadoop's home folder before testing. Must be version `1.0.3`.

## Compile
```
ant clean ; ant
```
## Run Remote
```
#!bash
./setup.sh run config/CONFIG_FILE
```
**Note:** Configuration files are located under `config` folder. Modify input and ouput variables as desired

## Run Locally
```
hadoop jar <jar> <main-class> <input-folder> <output-folder> <locally? yes/no> <number of files> <log directory>
```

* `<log directory>` is a directory inside the `<output folder>` and must not exist
* `<number of files>` variable is not used, but it is needed
* `<locally>` is intended to switch between S3 and HDFS, hence not being used when running on a local machine

Example:

```
hadoop jar main.jar applications.InvertedIndex input output yes 1 teste
```
